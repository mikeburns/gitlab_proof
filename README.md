# Keyoxide OpenPGP proof

[Verifying my OpenPGP key: openpgp4fpr:5fd82ce6a6463285538fc3a53e6761f72846b014].

This is used for [my Keyoxide profile](https://keyoxide.org/0x3E6761F72846B014).

The format is specified by [the Keyoxide spec](https://keyoxide.org/guides/gitlab).